<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatalogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatalogsTable Test Case
 */
class CatalogsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CatalogsTable
     */
    public $Catalogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.catalogs',
        'app.orders',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Catalogs') ? [] : ['className' => CatalogsTable::class];
        $this->Catalogs = TableRegistry::getTableLocator()->get('Catalogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Catalogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
