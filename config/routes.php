<?php

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use App\Route;


Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Vendors', 'action' => 'login']);
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    // //rota para enviar catálogos
    // $routes->connect('/send-catalogs/*', ['controller' => 'Catalogs', 'action' => 'get_catalogs']);

    // //rota para enviar produtos
    // $routes->connect('/send-products/*', ['controller' => 'Products', 'action' => 'get_products']);

    // //rota para receber pedidos
    // $routes->connect('/get-orders/*', ['controller' => 'Orders', 'action' => 'get_orders']);

    // $routes->connect('/vendors/', ['controller' => 'Vendors', 'action' => 'index']);

    // $routes->setExtensions(['json']);
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('api/v1', function (RouteBuilder $routes) {
    $routes->setExtensions(['json']);
    $routes->resources('Catalogs');
    $routes->resources('Clients');
    $routes->resources('Orders');
    $routes->resources('Products');
    $routes->resources('Vendors');
    Router::connect('/api/v1/vendors/register', ['controller' => 'Vendors', 'action' => 'add', 'prefix' => 'api/v1']);
    Router::connect('/api/v1/vendors/token', ['controller' => 'Vendors', 'action' => 'token', 'prefix' => 'api/v1']);

    $routes->fallbacks(DashedRoute::class);
});
