<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Form->postLink(
                __('Excluir'),
                ['action' => 'delete', $order->ref],
                ['confirm' => __('Are you sure you want to delete # {0}?', $order->ref)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar Pedidos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Catálogos'), ['controller' => 'Catalogs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Catálogo'), ['controller' => 'Catalogs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar Produtos'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Produto'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Cliente'), ['controller' => 'Clients', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar Vendedores'), ['controller' => 'Vendors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Vendedor'), ['controller' => 'Vendors', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="orders form large-9 medium-8 columns content">
    <?= $this->Form->create($order) ?>
    <fieldset>
        <legend><?= __('Editar Pedido') ?></legend>
        <?php
            echo $this->Form->control('catalog_id', ['options' => $catalogs]);
            echo $this->Form->control('product_id', ['options' => $products]);
            echo $this->Form->control('cod_product');
            echo $this->Form->control('valor');
            echo $this->Form->control('quantity');
            echo $this->Form->control('client_ref', ['options' => $clients]);
            echo $this->Form->control('vendor_id', ['options' => $vendors]);
            echo $this->Form->control('status', [
                'options' => [
                    'Pendente'      => 'Pendente',
                    'Em seraparação' => 'Em seraparação',
                    'Em transporte' => 'Em transporte',
                    'Entregue' => 'Entregue'
                  ], 
                ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
