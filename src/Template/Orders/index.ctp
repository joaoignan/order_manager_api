<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Clientes'), ['controller' => 'Clients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pedidos'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="orders index large-9 medium-8 columns content">
    <h3><?= __('Pedidos') ?></h3>
    <?= $this->Form->create("", ['type'=>'get']) ?>
        <?= $this->Form->control('keyword', ['label' => 'Buscar Pedido']); ?>
        <button>Buscar</button>
    <?= $this->Form->end() ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Catálogo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Status do Pedido') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Data/hora') ?></th>
                <th scope="col"><?= $this->Paginator->sort('valor') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Quantidade') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cliente') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Vendedor') ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($orders as $order): ?>
            <tr>
                <td><?= $order->has('catalog') ? $this->Html->link($order->catalog->name, ['controller' => 'Catalogs', 'action' => 'view', $order->catalog->catalog_id]) : '' ?></td>
                <td><?= h($order->status) ?></td>
                <td><?= h($order->currentdate) ?></td>
                <td><?= $this->Number->format($order->valor) ?></td>
                <td><?= $this->Number->format($order->quantity) ?></td>
                <td><?= $order->has('client') ? $this->Html->link($order->client->name, ['controller' => 'Clients', 'action' => 'view', $order->client->ref]) : '' ?></td>
                <td><?= $order->has('vendor') ? $this->Html->link($order->vendor->name, ['controller' => 'Vendors', 'action' => 'view', $order->vendor->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $order->ref]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $order->ref]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $order->ref], ['confirm' => __('Are you sure you want to delete # {0}?', $order->ref)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
    <!-- <?= json_encode($products) ?> -->
</div>
