<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Listar Pedidos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Catálogos'), ['controller' => 'Catalogs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Catálogo'), ['controller' => 'Catalogs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar Produtos'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Produto'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Cliente'), ['controller' => 'Clients', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar Vendedores'), ['controller' => 'Vendors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Vendedor'), ['controller' => 'Vendors', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="orders form large-9 medium-8 columns content">
    <?= $this->Form->create($order) ?>
    <fieldset>
        <legend><?= __('Novo Pedido') ?></legend>
        <!-- <?= json_encode($product) ?> -->

        <?php
            echo $this->Form->control('catalog_id', ['options' => $catalogs, 'label' => 'Código do Catálogo']);
            echo $this->Form->control('product_id', ['options' => $products, 'label' => 'Produto']);
            echo $this->Form->control('cod_product', ['label' => 'Código do Produto']);
            echo $this->Form->control('valor', ['label' => 'Preço']);
            echo $this->Form->control('quantity', ['label' => 'Quantidade']);
            echo $this->Form->control('client_ref', ['options' => $clients, 'label' => 'Cliente']);
            echo $this->Form->control('vendor_id', ['options' => $vendors, 'label' => 'Vendedor']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Adicionar')) ?>
    <?= $this->Form->end() ?>
</div>
