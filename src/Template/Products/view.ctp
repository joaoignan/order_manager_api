<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Edit Product'), ['action' => 'edit', $product->cod_product]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Product'), ['action' => 'delete', $product->cod_product], ['confirm' => __('Are you sure you want to delete # {0}?', $product->cod_product)]) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Catalogs'), ['controller' => 'Catalogs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Catalog'), ['controller' => 'Catalogs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="products view large-9 medium-8 columns content">
    <h3><?= h($product->cod_product) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Cod Product') ?></th>
            <td><?= h($product->cod_product) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Catalog') ?></th>
            <td><?= $product->has('catalog') ? $this->Html->link($product->catalog->name, ['controller' => 'Catalogs', 'action' => 'view', $product->catalog->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prod Name') ?></th>
            <td><?= h($product->prod_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= h($product->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Property') ?></th>
            <td><?= h($product->property) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Orders') ?></h4>
        <?php if (!empty($product->orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Catalog Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Cod Product') ?></th>
                <th scope="col"><?= __('Valor') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Client Id') ?></th>
                <th scope="col"><?= __('Vendor Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->orders as $orders): ?>
            <tr>
                <td><?= h($orders->ref) ?></td>
                <td><?= h($orders->catalog_id) ?></td>
                <td><?= h($orders->product_id) ?></td>
                <td><?= h($orders->cod_product) ?></td>
                <td><?= h($orders->valor) ?></td>
                <td><?= h($orders->quantity) ?></td>
                <td><?= h($orders->client_id) ?></td>
                <td><?= h($orders->vendor_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->ref]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Orders', 'action' => 'edit', $orders->ref]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Orders', 'action' => 'delete', $orders->ref], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->ref)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
