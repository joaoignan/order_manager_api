<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Listar Produtos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Catálogos'), ['controller' => 'Catalogs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Catálogo'), ['controller' => 'Catalogs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Listar Pedidos'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Pedido'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="netGroupCities form">
    <?= $this->Form->create(null, ['class' => 'form-horizontal bordered-row', 'type' => 'file']); ?>
    <fieldset>
        <legend><?= __('Importar Produtos do arquivo CSV') ?></legend>

        <br class="clear">
        <br class="clear">

        <div class="form-group">
            <div class="col-sm-2 control-label">
                <?= $this->Form->label('csv', 'Arquivo CSV') ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->control('csv', [
                    'type' => 'file',
                    'label' => false,
                ]) ?>
                Selecione um arquivo CSV com padrão de dados separados por ponto e vírgulas ";"
            </div>
        </div>

        <div class="form-group text-center">
             <?= $this->Form->button('<i class="glyph-icon icon-check-square-o"></i><span> '.__('Submit').'</span>',
                ['class' => 'btn btn-alt btn-hoverrr btn-default',  'style' =>  'width: 9.5em;',
                    'escape' => false]); ?>
        </div>

    </fieldset>
    <?= $this->Form->end() ?>
</div>