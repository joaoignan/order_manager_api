<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product[]|\Cake\Collection\CollectionInterface $products
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        
        <li><?= $this->Html->link(__('Novo'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Catálogos'), ['controller' => 'Catalogs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Importar CSV'), ['controller' => 'Products', 'action' => 'import_csv']) ?></li>
    </ul>
</nav>
<div class="products index large-9 medium-8 columns content">
    <h3><?= __('Produtos') ?></h3>
    <?= $this->Form->create("", ['type'=>'get']) ?>
        <?= $this->Form->control('keyword', ['label' => 'Buscar Produto']); ?>
        <button>Buscar</button>
    <?= $this->Form->end() ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('cod_product', ['label' => 'Código do Produto']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('catalog_id', ['label' => 'Catálogo']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('prod_name', ['label' => 'Nome']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('price', ['label' => 'Preço']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('property', ['label' => 'Característica']) ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product): ?>
            <tr>
                <td><?= h($product->cod_product) ?></td>
                <td><?= $product->has('catalog') ? $this->Html->link($product->catalog->name, ['controller' => 'Catalogs', 'action' => 'view', $product->catalog->id]) : '' ?></td>
                <td><?= h($product->prod_name) ?></td>
                <td><?= h($product->price) ?></td>
                <td><?= h($product->property) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $product->cod_product]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $product->cod_product]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->cod_product)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primeiro')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('próximo') . ' >') ?>
            <?= $this->Paginator->last(__('último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} cadastro(s) de {{count}}')]) ?></p>
    </div>
</div>
