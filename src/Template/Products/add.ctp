<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Produtos'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Vendedores'), ['controller' => 'Vendors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pedidos'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        
    </ul>
</nav>
<div class="products form large-9 medium-8 columns content">
    <?= $this->Form->create($product) ?>
    <fieldset>
        <legend><?= __('Adicionar Produto') ?></legend>
        <?php
            echo $this->Form->control('cod_product', ['label' => 'Código do Produto']);
            echo $this->Form->control('catalog_id', ['options' => $catalogs, 'label' => 'Catálogo']);

            // echo $this->Form->control('catalog_id', [
                // 'options' => [
                //     'Selecione'      => 'Selecione',
                //     'Conta Corrente' => 'Conta Corrente',
                //     'Conta Poupança' => 'Conta Poupança'
                //   ], 
            //     'label' => 'Catálogo']);
            
            echo $this->Form->control('prod_name', ['label' => 'Produto']);
            echo $this->Form->control('price', ['label' => 'Preço']);
            echo $this->Form->control('property', ['label' => 'Característica']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Cadastrar')) ?>
    <?= $this->Form->end() ?>
</div>
