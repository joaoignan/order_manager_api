<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Catalog $catalog
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Produtos'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Vendedores'), ['controller' => 'Vendors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pedidos'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="catalogs view large-9 medium-8 columns content">
    <h3><?= h($catalog->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($catalog->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($catalog->catalog_id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Pedidos') ?></h4>
        <?php if (!empty($catalog->orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Catalogo Id') ?></th>
                <th scope="col"><?= __('Produto Id') ?></th>
                <th scope="col"><?= __('Cod Produto') ?></th>
                <th scope="col"><?= __('Preço') ?></th>
                <th scope="col"><?= __('Quantidade') ?></th>
                <th scope="col"><?= __('Cliente Id') ?></th>
                <th scope="col"><?= __('Vendedor Id') ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($catalog->orders as $orders): ?>
            <tr>
                <td><?= h($orders->ref) ?></td>
                <td><?= h($orders->catalog_id) ?></td>
                <td><?= h($orders->product_id) ?></td>
                <td><?= h($orders->cod_product) ?></td>
                <td><?= h($orders->valor) ?></td>
                <td><?= h($orders->quantity) ?></td>
                <td><?= h($orders->client_id) ?></td>
                <td><?= h($orders->vendor_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver |'), ['controller' => 'Orders', 'action' => 'view', $orders->ref]) ?>
                    <?= $this->Html->link(__('Editar |'), ['controller' => 'Orders', 'action' => 'edit', $orders->ref]) ?>
                    <?= $this->Form->postLink(__('Deletar'), ['controller' => 'Orders', 'action' => 'delete', $orders->ref], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->ref)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Produtos') ?></h4>
        <?php if (!empty($catalog->products)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Produto') ?></th>
                <th scope="col"><?= __('Preço') ?></th>
                <th scope="col"><?= __('Característica') ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($catalog->products as $products): ?>
            <tr>
                <td><?= h($products->prod_name) ?></td>
                <td><?= h($products->price) ?></td>
                <td><?= h($products->property) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver |'), ['controller' => 'Products', 'action' => 'view', $products->cod_product]) ?>
                    <?= $this->Html->link(__('Editar |'), ['controller' => 'Products', 'action' => 'edit', $products->cod_product]) ?>
                    <?= $this->Form->postLink(__(' Deletar'), ['controller' => 'Products', 'action' => 'delete', $products->cod_product], ['confirm' => __('Are you sure you want to delete # {0}?', $products->cod_product)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
