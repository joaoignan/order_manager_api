<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Catalog[]|\Cake\Collection\CollectionInterface $catalogs
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Produtos'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Vendedores'), ['controller' => 'Vendors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Novo Catálogo'), ['controller' => 'Catalogs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Pedidos'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="catalogs index large-9 medium-8 columns content">
    <h3><?= __('Catálogos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('catalog_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name', ['label' => 'Nome']) ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($catalogs as $catalog): ?>
            <tr>
                <td><?= $this->Number->format($catalog->catalog_id) ?></td>
                <td><?= h($catalog->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver |'), ['action' => 'view', $catalog->catalog_id]) ?>
                    <?= $this->Html->link(__('Editar |'), ['action' => 'edit', $catalog->catalog_id]) ?>
                    <?= $this->Form->postLink(__('Excluir'), ['action' => 'delete', $catalog->catalog_id], ['confirm' => __('Are you sure you want to delete # {0}?', $catalog->catalog_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primeiro')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Próximo') . ' >') ?>
            <?= $this->Paginator->last(__('Último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} cadastros(s) de {{count}}')]) ?></p>
    </div>
</div>
