<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Catalog $catalog
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Menu') ?></li>
        <li><?= $this->Html->link(__('Produtos'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Vendedores'), ['controller' => 'Vendors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pedidos'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="catalogs form large-9 medium-8 columns content">
    <?= $this->Form->create($catalog) ?>
    <fieldset>
        <legend><?= __('Adicionar catálogo') ?></legend>
        <?php
            echo $this->Form->control('name', ['label' => 'Nome']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Cadastrar')) ?>
    <?= $this->Form->end() ?>
</div>
