<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Casa & Beleza';
?>
<!DOCTYPE html>
<html>

    <style>
    /* Slider
http://www.freshdesignweb.com/drop-down-responsive-menu-with-css3-and-jquery.html
*/
@import url(http://fonts.googleapis.com/css?family=Gentium+Basic:400,700|Yanone+Kaffeesatz);
/*===== nav style ======*/
#fdw nav select {
	display:none; /* this is just for the mobile display */
}
#fdw nav ul {
	display:block;
	z-index:999999;
}
#fdw nav ul li {
	display:inline-block;
	padding:50px 3px 30px;
	margin-left:30px;
	position:relative;
}
#fdw nav ul li a:link, #fdw nav ul li a:visited {
	color:#444;
	font:normal 20px 'Yanone Kaffeesatz', sans-serif;
	text-transform:uppercase;
	display:inline-block;
	position:relative;
}
#fdw nav ul li a:hover, #fdw nav ul li a:active {
	color:#e25d29;
	text-decoration:none;
}
#fdw nav ul li span {
	position:absolute;
	right:-12px;
	bottom:6px;
	width:7px;
	height:8px;
	margin:0 0 0 3px;
	float:right;
	display:block;
	background:url('images/nav_arrow.png') no-repeat left -8px;
	font:0/0 a;
}
#fdw nav ul li.current {
	border-bottom:2px solid #e25d29;
}
#fdw nav ul li.current a {
	color:#e25d29;
	cursor: default;
}
#fdw nav ul li.current a span {
	background:url('../images/nav_arrow.png') no-repeat left 0;
}
#fdw nav ul li.current ul li a {
	cursor:pointer;
}

/*===== sub_menu Style =======*/
#fdw nav ul li ul.sub_menu {
	position:absolute;
	top:90px;
	left:0;
	margin:0;
	padding:0;
	background:#fff;
	border:1px solid #ececec;
	border-top:5px solid #e25d29;
	display:none;
	z-index:999999;
    -moz-box-shadow: 0px 6px 7px #121012;
    -webkit-box-shadow: 0px 6px 7px #121012;
    box-shadow: 0px 6px 7px #121012;
}
#fdw nav ul li ul.sub_menu li.arrow_top {
	position:absolute;
	top:-12px;
	left:12px;
	width:13px;
	height:8px;
	display:block;
	border:none;
	background:url('images/arrow_top.png') no-repeat left top;
}
#fdw nav ul li ul.sub_menu li {
	float:none;
	margin:0;
	padding:0;
	border-bottom:1px solid #ececec;
}
#fdw nav ul li ul.sub_menu li a {
	white-space: nowrap;
	width: 150px;
	padding:12px;
	font:13px Arial, tahoma, sans-serif;
	text-transform:capitalize;
	color:#777;
}
#fdw nav ul li ul.sub_menu li a:hover {
	background:#f9f9f9;
	color:#333;
}
#fdw nav ul li ul.sub_menu li a.subCurrent {
	color:#e25d29;
	cursor:default;
}
#fdw nav ul li ul.sub_menu li a.subCurrent:hover {
	background:none;
}
/*===================== end Header style ======================*/


@media only screen and (min-width: 768px) and (max-width: 959px) {
		/* nav */
		#fdw nav ul li{
			margin-left:12px;
		}
}

	/* All Mobile Sizes (devices and browser) */
@media only screen and (max-width: 767px) {

		/* nav menu ul & select */
		#fdw nav ul {
			display:none;
		}
		#fdw nav select {
			width:100%;
			display:block;
			margin-bottom:30px;
			cursor:pointer;
			padding:6px;
			background:#f9f9f9;
			border:1px solid #e3e3e3;
			color:#777;
		}
}
    
    </style>

<script src="js/jquery-1.7.2.min.js"></script>
<!--  javaScript -->
<script>  
<!--  // building select nav for mobile width only -->
$(function(){
	// building select menu
	$('<select />').appendTo('nav');

	// building an option for select menu
	$('<option />', {
		'selected': 'selected',
		'value' : '',
		'text': 'Choise Page...'
	}).appendTo('nav select');

	$('nav ul li a').each(function(){
		var target = $(this);

		$('<option />', {
			'value' : target.attr('href'),
			'text': target.text()
		}).appendTo('nav select');

	});

	// on clicking on link
	$('nav select').on('change',function(){
		window.location = $(this).find('option:selected').val();
	});
});

// show and hide sub menu
$(function(){
	$('nav ul li').hover(
		function () {
			//show its submenu
			$('ul', this).slideDown(150);
		}, 
		function () {
			//hide its submenu
			$('ul', this).slideUp(150);			
		}
	);
});
//end
</script>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href="/vendors"><?= "Casa & Beleza" ?></a></h1>
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                <!-- <li><a target="_blank" href="https://book.cakephp.org/3.0/">Documentation</a></li>
                <li><a target="_blank" href="https://api.cakephp.org/3.0/">API</a></li> -->
                <?php if ($mobile): ?>
                <li><?= $this->Html->link(__('Sair'), ['controller' => 'Vendors', 'action' => 'logout']) ?></li>
                
                <?php endif; ?>
            </ul>
        </div>

        
    </nav>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>
