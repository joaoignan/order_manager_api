<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Catalogs']
        ];
        $keyword = $this->request->query('keyword');
        if(!empty($keyword)){
            $this->paginate = [
                'conditions'=>['prod_name LIKE '=>'%'.$keyword.'%']
            ];
        }
        $products = $this->paginate($this->Products);

        $this->set(compact('products'));
    }

    /**
     * View method
     *
     * @param string|null $cod_product Product cod_product.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($cod_product = null)
    {
        $product = $this->Products->get($cod_product, [
            'contain' => ['Catalogs', 'Orders']
        ]);

        $this->set('product', $product);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $catalogs = $this->Products->Catalogs->find('list', ['limit' => 200]);
        $this->set(compact('product', 'catalogs'));
    }

    /**
     * Edit method
     *
     * @param string|null $cod_product Product cod_product.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($cod_product = null)
    {
        $product = $this->Products->get($cod_product, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $catalogs = $this->Products->Catalogs->find('list', ['limit' => 200]);
        $this->set(compact('product', 'catalogs'));
    }

    /**
     * Delete method
     *
     * @param string|null $cod_product Product cod_product.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($cod_product = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($cod_product);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    
    public function importCsv(){

        if ($this->request->is('post')) {

            $handle = fopen($this->request->data['csv']['tmp_name'], "r");

            if ($handle) {
                while (($data = fgetcsv($handle, 1000, ";")) !== false) {
                    // $line = array_map('utf8_encode', $line);

                    $dataSave = [
                        'cod_product'    => $data[0],
                        'catalog_id'     => $data[1],
                        'prod_name'      => $data[2],
                        'price'          => $data[3],
                        'property'       => $data[4],
                    ];

                    $produto_save = $this->Products->newEntity();
                    $produto_save = $this->Products->patchEntity($produto_save, $dataSave);

                    if ($this->Products->save($produto_save)) {
                        $this->Flash->success(__('The product has been saved.'));
                    } else {
                        $this->Flash->error(__('The product could not be saved. Please, try again.'));
                    }

                }
            }else{
                $this->Flash->error('Falha ao carregar arquivo CSV. Tente novamente...');
                $this->redirect(['action' => 'importCsv']);
            }
            fclose($handle);

            $this->redirect(['action' => 'importCsv']);
        }
    }

    public function search() {
        if (!isset($this->request->query['keywords'])) {
            throw new BadRequestException();
        }

        $results = $this->Location->findByKeywords($this->request->query['keywords']);

        $this->set('results', $results);
    }
}
