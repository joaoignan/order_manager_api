<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // $this->paginate = [
        //     'contain' => ['Catalogs', 'Products', 'Clients', 'Vendors']
        // ];

        $this->paginate = [
            'contain' => ['Catalogs', 'Clients', 'Vendors']
        ];
        $keyword = $this->request->query('keyword');
        if(!empty($keyword)){
            $this->paginate = [
                'conditions'=>['currentdate LIKE '=>'%'.$keyword.'%']
            ];
        }

        $products = $this->Orders->Products->find('all');

        $orders = $this->paginate($this->Orders);

        

        $this->set(compact('orders', 'products'));
    }

    /**
     * View method
     *
     * @param string|null $ref Order ref.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($ref = null)
    {
        $order = $this->Orders->get($ref, [
            'contain' => ['Catalogs', 'Clients', 'Vendors']
        ]);

        $this->set('order', $order);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $catalogs = $this->Orders->Catalogs->find('list', ['limit' => 200]);
        $products = $this->Orders->Products->find('list', ['limit' => 200]);
        $clients = $this->Orders->Clients->find('list', ['limit' => 200]);
        $vendors = $this->Orders->Vendors->find('list', ['limit' => 200]);

        $this->set(compact('order', 'catalogs', 'products', 'clients', 'vendors', 'product'));
    }

    /**
     * Edit method
     *
     * @param string|null $ref Order ref.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($ref = null)
    {
        $order = $this->Orders->get($ref, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $catalogs = $this->Orders->Catalogs->find('list', ['limit' => 200]);
        $products = $this->Orders->Products->find('list', ['limit' => 200]);
        $clients = $this->Orders->Clients->find('list', ['limit' => 200]);
        $vendors = $this->Orders->Vendors->find('list', ['limit' => 200]);
        $this->set(compact('order', 'catalogs', 'products', 'clients', 'vendors'));
    }

    /**
     * Delete method
     *
     * @param string|null $ref Order ref.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($ref = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($ref);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
