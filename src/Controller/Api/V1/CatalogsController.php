<?php
namespace App\Controller\Api\V1;
use App\Controller\Api\V1\AppController;


/**
 * Catalogs Controller
 *
 * @property \App\Model\Table\CatalogsTable $Catalogs
 *
 * @method \App\Model\Entity\Catalog[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CatalogsController extends AppController
{
    use \Crud\Controller\ControllerTrait;

    //API_FUNCTIONS

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    // public function index()
    // {
    //     $catalogs = $this->Catalogs->find('all');
    //     $this->set([
    //         'catalogs' => $catalogs,
    //         '_serialize' => ['catalogs']
    //     ]);
    // }

    // public function view($id)
    // {
    //     $catalog = $this->Catalogs->get($id);
    //     $this->set([
    //         'catalog' => $catalog,
    //         '_serialize' => ['catalog']
    //     ]);
    // }

    // public function add()
    // {
    //     $catalog = $this->Catalogs->newEntity($this->request->getData());
    //     if ($this->Catalogs->save($catalog)) {
    //         $message = 'Saved';
    //     } else {
    //         $message = 'Error';
    //     }
    //     $this->set([
    //         'message' => $message,
    //         'catalog' => $catalog,
    //         '_serialize' => ['message', 'catalog']
    //     ]);
    // }

    // public function edit($id)
    // {
    //     $catalog = $this->Catalogs->get($id);
    //     if ($this->request->is(['post', 'put'])) {
    //         $catalog = $this->Catalogs->patchEntity($catalog, $this->request->getData());
    //         if ($this->Catalogs->save($catalog)) {
    //             $message = 'Saved';
    //         } else {
    //             $message = 'Error';
    //         }
    //     }
    //     $this->set([
    //         'message' => $message,
    //         '_serialize' => ['message']
    //     ]);
    // }

    // public function delete($id)
    // {
    //     $catalog = $this->Catalogs->get($id);
    //     $message = 'Deleted';
    //     if (!$this->Catalogs->delete($catalog)) {
    //         $message = 'Error';
    //     }
    //     $this->set([
    //         'message' => $message,
    //         '_serialize' => ['message']
    //     ]);
    // }
}
