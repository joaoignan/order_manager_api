<?php

namespace App\Controller\Api\V1;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppController extends Controller
{
    use \Crud\Controller\ControllerTrait;
    public function initialize()
    {
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Crud.Crud', [
            'actions' => [
                'Crud.Index',
                'Crud.View',
                'Crud.Add',
                'Crud.Edit',
                'Crud.Delete',
            ],
            'listeners' => [
                // 'CrudJsonApi.JsonApi',
                'Crud.Api',
                // 'Crud.ApiPagination'
                // 'Crud.ApiQueryLog',
                // 'Crud.Search'
            ]
        ]);

        // if ($this->request->getParam('controller') == 'Vendors') {
        //     $this->Crud->addListener('Crud.Api');
        // } else {
        //     $this->Crud->addListener('Crud.Api');
        // }

        $this->loadComponent('Auth', [
            'storage' => 'Memory',
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Vendors',
                    'fields' => [
                        'username' => 'mobile',
                        'password' => 'password'
                    ],
                ],
                'ADmad/JwtAuth.Jwt' => [
                    'parameter' => 'token',
                    'userModel' => 'Vendors',
                    'fields' => [
                        'username' => 'id'
                    ],
                    'queryDatasource' => true
                ]
            ],
            'unauthorizedRedirect' => false,
            'checkAuthIn' => 'Controller.initialize'
        ]);

    }



}
