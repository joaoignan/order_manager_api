<?php
namespace App\Controller\Api\V1;
use App\Controller\Api\V1\AppController;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
        //API_FUNCTIONS
        use \Crud\Controller\ControllerTrait;

    // public function initialize()
    // {
    //     parent::initialize();
    //     $this->loadComponent('RequestHandler');
    // }

    // public function index()
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $products = $this->Products->find('all');
    //     $this->set([
    //         'products' => $products,
    //         '_serialize' => ['products']
    //     ]);
    // }

    // public function view($id)
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $product = $this->Products->get($id);
    //     $this->set([
    //         'product' => $product,
    //         '_serialize' => ['product']
    //     ]);
    // }

    // public function add()
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $product = $this->Products->newEntity($this->request->getData());
    //     if ($this->Products->save($product)) {
    //         $message = 'Saved';
    //     } else {
    //         $message = 'Error';
    //     }
    //     $this->set([
    //         'message' => $message,
    //         'product' => $product,
    //         '_serialize' => ['message', 'product']
    //     ]);
    // }

    // public function edit($id)
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $product = $this->Products->get($id);
    //     if ($this->request->is(['post', 'put'])) {
    //         $product = $this->Products->patchEntity($product, $this->request->getData());
    //         if ($this->Products->save($product)) {
    //             $message = 'Saved';
    //         } else {
    //             $message = 'Error';
    //         }
    //     }
    //     $this->set([
    //         'message' => $message,
    //         '_serialize' => ['message']
    //     ]);
    // }

    // public function delete($id)
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $product = $this->Products->get($id);
    //     $message = 'Deleted';
    //     if (!$this->Products->delete($product)) {
    //         $message = 'Error';
    //     }
    //     $this->set([
    //         'message' => $message,
    //         '_serialize' => ['message']
    //     ]);
    // }
}
