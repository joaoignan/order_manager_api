<?php
namespace App\Controller\Api\V1;
use App\Controller\Api\V1\AppController;

/**
 * Clients Controller
 *
 * @property \App\Model\Table\ClientsTable $Clients
 *
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClientsController extends AppController
{
    //API_FUNCTIONS
    use \Crud\Controller\ControllerTrait;

    // public function initialize()
    // {
    //     parent::initialize();
    //     $this->loadComponent('RequestHandler');
    // }

    // public function index()
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $clients = $this->Clients->find('all');
    //     $this->set([
    //         'clients' => $clients,
    //         '_serialize' => ['clients']
    //     ]);
    // }

    // public function view($id)
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $client = $this->Clients->get($id);
    //     $this->set([
    //         'client' => $client,
    //         '_serialize' => ['client']
    //     ]);
    // }

    // public function add()
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $client = $this->Clients->newEntity($this->request->getData());
    //     if ($this->Clients->save($client)) {
    //         $message = 'Saved';
    //     } else {
    //         $message = 'Error';
    //     }
    //     $this->set([
    //         'message' => $message,
    //         'client' => $client,
    //         '_serialize' => ['message', 'client']
    //     ]);
    // }

    // public function edit($id)
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $client = $this->Clients->get($id);
    //     if ($this->request->is(['post', 'put'])) {
    //         $client = $this->Clients->patchEntity($client, $this->request->getData());
    //         if ($this->Clients->save($client)) {
    //             $message = 'Saved';
    //         } else {
    //             $message = 'Error';
    //         }
    //     }
    //     $this->set([
    //         'message' => $message,
    //         '_serialize' => ['message']
    //     ]);
    // }

    // public function delete($id)
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $client = $this->Clients->get($id);
    //     $message = 'Deleted';
    //     if (!$this->Clients->delete($client)) {
    //         $message = 'Error';
    //     }
    //     $this->set([
    //         'message' => $message,
    //         '_serialize' => ['message']
    //     ]);
    // }

}
