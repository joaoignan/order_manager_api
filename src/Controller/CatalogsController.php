<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Catalogs Controller
 *
 * @property \App\Model\Table\CatalogsTable $Catalogs
 *
 * @method \App\Model\Entity\Catalog[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CatalogsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $catalogs = $this->paginate($this->Catalogs);

        $this->set(compact('catalogs'));
    }

    /**
     * View method
     *
     * @param string|null $catalog_id Catalog catalog_id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($catalog_id = null)
    {
        $catalog = $this->Catalogs->get($catalog_id, [
            'contain' => ['Orders', 'Products']
        ]);

        $this->set('catalog', $catalog);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $catalog = $this->Catalogs->newEntity();
        if ($this->request->is('post')) {
            $catalog = $this->Catalogs->patchEntity($catalog, $this->request->getData());
            if ($this->Catalogs->save($catalog)) {
                $this->Flash->success(__('The catalog has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The catalog could not be saved. Please, try again.'));
        }
        $this->set(compact('catalog'));
    }

    /**
     * Edit method
     *
     * @param string|null $catalog_id Catalog catalog_id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($catalog_id = null)
    {
        $catalog = $this->Catalogs->get($catalog_id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $catalog = $this->Catalogs->patchEntity($catalog, $this->request->getData());
            if ($this->Catalogs->save($catalog)) {
                $this->Flash->success(__('The catalog has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The catalog could not be saved. Please, try again.'));
        }
        $this->set(compact('catalog'));
    }

    /**
     * Delete method
     *
     * @param string|null $catalog_id Catalog catalog_id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($catalog_id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $catalog = $this->Catalogs->get($catalog_id);
        if ($this->Catalogs->delete($catalog)) {
            $this->Flash->success(__('The catalog has been deleted.'));
        } else {
            $this->Flash->error(__('The catalog could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //API_FUNCTIONS

    // public function initialize()
    // {
    //     parent::initialize();
    //     $this->loadComponent('RequestHandler');
    // }

    // public function index_catalogs()
    // {
    //     $catalogs = $this->Catalogs->find('all');
    //     $this->set([
    //         'catalogs' => $catalogs,
    //         '_serialize' => ['catalogs']
    //     ]);
    // }

    // public function view_catalog($id)
    // {
    //     $catalog = $this->Catalogs->get($id);
    //     $this->set([
    //         'catalog' => $catalog,
    //         '_serialize' => ['catalog']
    //     ]);
    // }

    // public function add_catalog()
    // {
    //     $catalog = $this->Catalogs->newEntity($this->request->getData());
    //     if ($this->Catalogs->save($catalog)) {
    //         $message = 'Saved';
    //     } else {
    //         $message = 'Error';
    //     }
    //     $this->set([
    //         'message' => $message,
    //         'catalog' => $catalog,
    //         '_serialize' => ['message', 'catalog']
    //     ]);
    // }

    // public function edit_catalog($id)
    // {
    //     $catalog = $this->Catalogs->get($id);
    //     if ($this->request->is(['post', 'put'])) {
    //         $catalog = $this->Catalogs->patchEntity($catalog, $this->request->getData());
    //         if ($this->Catalogs->save($catalog)) {
    //             $message = 'Saved';
    //         } else {
    //             $message = 'Error';
    //         }
    //     }
    //     $this->set([
    //         'message' => $message,
    //         '_serialize' => ['message']
    //     ]);
    // }

    // public function delete_catalog($id)
    // {
    //     $catalog = $this->Catalogs->get($id);
    //     $message = 'Deleted';
    //     if (!$this->Catalogs->delete($catalog)) {
    //         $message = 'Error';
    //     }
    //     $this->set([
    //         'message' => $message,
    //         '_serialize' => ['message']
    //     ]);
    // }
}
