<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Clients Controller
 *
 * @property \App\Model\Table\ClientsTable $Clients
 *
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClientsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Vendors']
        ];
        $clients = $this->paginate($this->Clients);

        $this->set(compact('clients'));
    }

    /**
     * View method
     *
     * @param string|null $ref Client ref.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($ref = null)
    {
        $client = $this->Clients->get($ref, [
            'contain' => ['Vendors', 'Orders']
        ]);

        $this->set('client', $client);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $client = $this->Clients->newEntity();
        if ($this->request->is('post')) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('The client has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The client could not be saved. Please, try again.'));
        }
        $vendors = $this->Clients->Vendors->find('list', ['limit' => 200]);
        $this->set(compact('client', 'vendors'));
    }

    /**
     * Edit method
     *
     * @param string|null $ref Client ref.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($ref = null)
    {
        $client = $this->Clients->get($ref, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('The client has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The client could not be saved. Please, try again.'));
        }
        $vendors = $this->Clients->Vendors->find('list', ['limit' => 200]);
        $this->set(compact('client', 'vendors'));
    }

    /**
     * Delete method
     *
     * @param string|null $ref Client ref.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($ref = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $client = $this->Clients->get($ref);
        if ($this->Clients->delete($client)) {
            $this->Flash->success(__('The client has been deleted.'));
        } else {
            $this->Flash->error(__('The client could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    // //API_FUNCTIONS

    // public function initialize()
    // {
    //     parent::initialize();
    //     $this->loadComponent('RequestHandler');
    // }

    // public function index_clients()
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $clients = $this->Clients->find('all');
    //     $this->set([
    //         'clients' => $clients,
    //         '_serialize' => ['clients']
    //     ]);
    // }

    // public function view_client($ref)
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $client = $this->Clients->get($ref);
    //     $this->set([
    //         'client' => $client,
    //         '_serialize' => ['client']
    //     ]);
    // }

    // public function add_client()
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $client = $this->Clients->newEntity($this->request->getData());
    //     if ($this->Clients->save($client)) {
    //         $message = 'Saved';
    //     } else {
    //         $message = 'Error';
    //     }
    //     $this->set([
    //         'message' => $message,
    //         'client' => $client,
    //         '_serialize' => ['message', 'client']
    //     ]);
    // }

    // public function edit_client($ref)
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $client = $this->Clients->get($ref);
    //     if ($this->request->is(['post', 'put'])) {
    //         $client = $this->Clients->patchEntity($client, $this->request->getData());
    //         if ($this->Clients->save($client)) {
    //             $message = 'Saved';
    //         } else {
    //             $message = 'Error';
    //         }
    //     }
    //     $this->set([
    //         'message' => $message,
    //         '_serialize' => ['message']
    //     ]);
    // }

    // public function delete_client($ref)
    // {
    //     header("Access-Control-Allow-Origin: *");
    //     $client = $this->Clients->get($ref);
    //     $message = 'Deleted';
    //     if (!$this->Clients->delete($client)) {
    //         $message = 'Error';
    //     }
    //     $this->set([
    //         'message' => $message,
    //         '_serialize' => ['message']
    //     ]);
    // }

}
