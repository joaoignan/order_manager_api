<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property string $cod_product
 * @property string $catalog_id
 * @property string $prod_name
 * @property string $price
 * @property string $property
 *
 * @property \App\Model\Entity\Catalog $catalog
 * @property \App\Model\Entity\Order[] $orders
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cod_product' => true,
        'catalog_id' => true,
        'prod_name' => true,
        'price' => true,
        'property' => true,
        'catalog' => true,
        'orders' => true
    ];
}
