<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Client Entity
 *
 * @property string $ref
 * @property int $vendor_id
 * @property string $name
 * @property string $telephone
 * @property string $data_atual
 *
 * @property \App\Model\Entity\Vendor $vendor
 * @property \App\Model\Entity\Order[] $orders
 */
class Client extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    // protected $_accessible = [
    //     'ref' => true,
    //     'vendor_id' => true,
    //     'name' => true,
    //     'telephone' => true,
    //     'vendor' => true,
    //     'orders' => true,
    //     'data_atual' => true
        
    // ];
}
