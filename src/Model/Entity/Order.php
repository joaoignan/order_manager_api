<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $ref
 * @property string $catalog_id
 * @property string $cod_product
 * @property float $valor
 * @property int $quantity
 * @property string $client_ref
 * @property int $vendor_id
 *
 * @property \App\Model\Entity\Catalog $catalog
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Client $client
 * @property \App\Model\Entity\Vendor $vendor
 */
class Order extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ref' => true,
        'catalog_id' => true,
        'cod_product' => true,
        'valor' => true,
        'quantity' => true,
        'client_ref' => true,
        'vendor_id' => true,
        'catalog' => true,
        'product' => true,
        'client' => true,
        'vendor' => true,
        'paid' => true,
        'currentdate' => true,
        'status' => true
    ];
}
