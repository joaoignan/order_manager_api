<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Catalogs Model
 *
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\ProductsTable|\Cake\ORM\Association\HasMany $Products
 *
 * @method \App\Model\Entity\Catalog get($primaryKey, $options = [])
 * @method \App\Model\Entity\Catalog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Catalog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Catalog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Catalog|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Catalog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Catalog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Catalog findOrCreate($search, callable $callback = null, $options = [])
 */
class CatalogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('catalogs');
        $this->setDisplayField('name');
        $this->setPrimaryKey('catalog_id');

        $this->hasMany('Orders', [
            'foreignKey' => 'catalog_id'
        ]);
        $this->hasMany('Products', [
            'foreignKey' => 'catalog_id'
        ]);

        $this->addBehavior('Search.Search');

        $this->searchManager()
            ->add('q', 'Search.Like', [
                'before'=>true,
                'after'=>true,
                'fieldMode'=>'OR',
                'comparisan'=>'like',
                'wildcardAny'=>'*',
                'wildcardOne'=>'?',
                'field'=>['name'],
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('catalog_id')
            ->allowEmpty('catalog_id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 30)
            ->allowEmpty('name');

        return $validator;
    }
}
